Coffee Maker
============

System Structure:
----------------

(Class diagram available in the class_diagram.png file)

##### 1. Sensors
	There are 3 physical sensors used in the coffee maker: they all perform the same function (measuring the weights of the contents of a Tank element) and so they are unified under a single WeightSensor class,
	which is further described by the ISensor abstract class/interface.
##### 2. Tanks
	Similarly to the sensors, the tanks used in the system are all identical in nature - they hold a resource they can dump a specific amount of and have a Sensor that contains information on the weight of the contents.
	As such, the interface ITank is used to describe their behaviour. The WaterTank, MilkTank and CoffeeBeansTank all implement the ITank interface.
##### 3. Grinder
	Before you can brew coffee, you have to grind the beans. At first glance, the grinder behaves similarly to a Tank - you request a certain amount of grounds and get them from the Grinder. The difference lies in
	the fact that the Grinder has no Sensor, in turn it has an associated CoffeeBeansTank and communicates with it to receive the appropriate amount of coffee beans for grinding.
##### 4. Brewer
	The brewer is connected to a grinder, a milk tank and a water tank. The brewer is in charge of requisitioning all the resources (barring the beans which only the grinder has access to) and brewing them together.
##### 5. Controller
	There is a simple controller class that relays info between the Brewer and the MainView. The controller adds an extra level of abstraction which felt adequate - connecting the brewer to the view directly doesn't seem like
	a very elegant solution. The other reason for introducing a Controller layer is the existence of the CoffeeTypes resource, which holds the names of the available coffee names and their ingredients.
##### 6. CoffeeTypesDictionary
	The coffee types dictionary contains coffee names and ingredients. It is described by the ICoffeeTypesDictionary interface.
##### 7. CoffeeMainView
	A very simple View that allows you to choose your preferred beverage type and begin the brewing process.
	
Testing:
--------

the /test directory contains unit tests for all the modules described above with the exception of the controller, the main view and the coffee types resource. The tests include edge cases, invalid input cases, cases that raise errors.

