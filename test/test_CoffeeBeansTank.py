from coffeemaker.src.model.WeightSensor import WeightSensor
from coffeemaker.src.model.CoffeeBeansTank import CoffeeBeansTank
from coffeemaker.src.model.InsufficientResourceError import InsufficientResourceError
import unittest


class test_CoffeeBeansTank(unittest.TestCase):
    def setUp(self):
        self.test_coffee_beans_sensor = WeightSensor()
        self.test_coffee_beans_tank = CoffeeBeansTank(self.test_coffee_beans_sensor)

    def test_coffee_beans_tank_default(self):
        self.assertEqual(self.test_coffee_beans_tank.max_content_weight, 250)
        self.assertEqual(self.test_coffee_beans_tank.coffee_beans_sensor.get_reading(), 0)

    def test_coffee_beans_tank_get_contents(self):
        self.test_coffee_beans_sensor.current_reading = 200
        self.test_coffee_beans_tank.get_contents(200)
        self.test_coffee_beans_tank.get_contents(0)

    def test_coffee_beans_tank_get_excessive_contents(self):
        with self.assertRaises(InsufficientResourceError):
            self.test_coffee_beans_tank.get_contents(201)

    def test_coffee_beans_tank_get_negative(self):
        with self.assertRaises(ValueError):
            self.test_coffee_beans_tank.get_contents(-1)
