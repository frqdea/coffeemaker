from coffeemaker.src.model.WeightSensor import WeightSensor
from coffeemaker.src.model.MilkTank import MilkTank
from coffeemaker.src.model.InsufficientResourceError import InsufficientResourceError
import unittest


class test_MilkTank(unittest.TestCase):
    def setUp(self):
        self.test_milk_sensor = WeightSensor()
        self.test_milk_tank = MilkTank(self.test_milk_sensor)

    def test_milk_tank_default(self):
        self.assertEqual(self.test_milk_tank.max_content_weight, 500)
        self.assertEqual(self.test_milk_tank.milk_sensor.get_reading(), 0)

    def test_milk_tank_get_contents(self):
        self.test_milk_sensor.current_reading = 200
        self.test_milk_tank.get_contents(0)
        self.test_milk_tank.get_contents(200)

    def test_milk_tank_get_excessive_contents(self):
        with self.assertRaises(InsufficientResourceError):
            self.test_milk_tank.get_contents(201)

    def test_milk_tank_get_negative_contents(self):
        with self.assertRaises(ValueError):
            self.test_milk_tank.get_contents(-1)
