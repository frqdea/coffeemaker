from coffeemaker.src.model.WeightSensor import WeightSensor
from coffeemaker.src.model.WaterTank import WaterTank
from coffeemaker.src.model.InsufficientResourceError import InsufficientResourceError
import unittest


class test_WaterTank(unittest.TestCase):
    def setUp(self):
        self.test_water_sensor = WeightSensor()
        self.test_water_tank = WaterTank(self.test_water_sensor)

    def test_water_tank_default(self):
        self.assertEqual(self.test_water_tank.max_content_weight, 1000)
        self.assertEqual(self.test_water_tank.water_sensor.get_reading(), 0)

    def test_water_tank_get_contents(self):
        self.test_water_sensor.current_reading = 200
        self.test_water_tank.get_contents(200)
        self.test_water_tank.get_contents(0)

    def test_water_tank_get_excessive_contents(self):
        with self.assertRaises(InsufficientResourceError):
            self.test_water_tank.get_contents(201)

    def test_water_tank_get_negative_contents(self):
        with self.assertRaises(ValueError):
            self.test_water_tank.get_contents(-1)
