from coffeemaker.src.model.WeightSensor import WeightSensor
from coffeemaker.src.model.CoffeeBeansTank import CoffeeBeansTank
from coffeemaker.src.model.WaterTank import WaterTank
from coffeemaker.src.model.MilkTank import MilkTank
from coffeemaker.src.model.InsufficientResourceError import InsufficientResourceError
from coffeemaker.src.model.Grinder import Grinder
from coffeemaker.src.model.Brewer import Brewer

import unittest


class test_Brewer(unittest.TestCase):
    def setUp(self):
        self.test_coffee_beans_sensor = WeightSensor(200)
        self.test_coffee_beans_tank = CoffeeBeansTank(self.test_coffee_beans_sensor)

        self.test_water_sensor = WeightSensor(200)
        self.test_water_tank = WaterTank(self.test_water_sensor)

        self.test_milk_sensor = WeightSensor(200)
        self.test_milk_tank = MilkTank(self.test_milk_sensor)

        self.test_grinder = Grinder(self.test_coffee_beans_tank)
        self.test_brewer = Brewer(self.test_water_tank, self.test_milk_tank, self.test_grinder)

        self.test_dict = {
                      "correct": {
                        "water": 100,
                        "grounds": 50,
                        "milk": 0
                      },
                      "negative": {
                        "water": -1,
                        "grounds": 0,
                        "milk": 0
                      },
                      "insufficient": {
                        "water": 201,
                        "grounds": 0,
                        "milk": 0
                      },
                      "key_error": {
                        "?": 0,
                      },
                    }

    def test_brewer_brew_correct_coffee(self):
        self.test_brewer.brew(self.test_dict["correct"])

    def test_brewer_brew_negative_coffee(self):
        with self.assertRaises(ValueError):
            self.test_brewer.brew(self.test_dict["negative"])

    def test_brewer_brew_insufficient_coffee(self):
        with self.assertRaises(InsufficientResourceError):
            self.test_brewer.brew(self.test_dict["insufficient"])

    def test_brewer_brew_key_error_coffee(self):
        with self.assertRaises(KeyError):
            self.test_brewer.brew(self.test_dict["key_error"])
