from coffeemaker.src.model.WeightSensor import WeightSensor
from coffeemaker.src.model.CoffeeBeansTank import CoffeeBeansTank
from coffeemaker.src.model.InsufficientResourceError import InsufficientResourceError
from coffeemaker.src.model.Grinder import Grinder

import unittest


class test_Grinder(unittest.TestCase):
    def setUp(self):
        test_coffee_beans_sensor = WeightSensor(100)
        test_coffee_beans_tank = CoffeeBeansTank(test_coffee_beans_sensor)
        self.test_grinder = Grinder(test_coffee_beans_tank)

    def test_grinder_correct(self):
        self.test_grinder.get_coffee_grounds(0)
        self.test_grinder.get_coffee_grounds(100)

    def test_grinder_excessive(self):
        with self.assertRaises(InsufficientResourceError):
            self.test_grinder.get_coffee_grounds(201)

    def test_grinder_negative(self):
        with self.assertRaises(ValueError):
            self.test_grinder.get_coffee_grounds(-1)
