from coffeemaker.src.model.WeightSensor import WeightSensor
import unittest


class test_WeightSensor(unittest.TestCase):
    def test_weight_sensor_default(self):
        test_sensor = WeightSensor()

        self.assertEqual(test_sensor.get_reading(), 0)

    def test_weight_sensor_change_reading(self):
        test_sensor = WeightSensor()
        test_sensor.current_reading = 1

        self.assertEqual(test_sensor.get_reading(), 1)

    def test_weight_sensor_negative_reading(self):
        test_sensor = WeightSensor(-1)

        with self.assertRaises(ValueError):
            test_sensor.get_reading()
