from tkinter import *
from tkinter import messagebox


class CoffeeMainView:
    """
    Main View. Displays the list of coffee types and allows you to brew a beverage of the client's choosing.
    """

    def __init__(self, coffee_main_view_controller):
        self.coffee_main_view_controller = coffee_main_view_controller

    def main_menu(self, coffee_types):
        """
        Display the main view containing the list of available coffee types.
        Send the user's choice to the controller.
        :param coffee_types:
        :return:
        """
        master = Tk()

        coffee_types_stringvar = StringVar(master)
        coffee_types_stringvar.set(coffee_types[0])

        coffee_types_dropdown = OptionMenu(master, coffee_types_stringvar, *coffee_types)
        coffee_types_dropdown.pack(padx=50, pady=50)

        def on_click():
            if self.coffee_main_view_controller.send_coffee_type(coffee_types_stringvar.get()):
                message = "Coffee brewed successfully!"
            else:
                message = "Coffee brewing failed :("
            messagebox.showinfo("Coffee status", message)

        brew_button = Button(master, text="Brew!", command=on_click)
        brew_button.pack(padx=50, pady=25)

        mainloop()
