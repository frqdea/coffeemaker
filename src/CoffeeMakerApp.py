from coffeemaker.src.model.Brewer import Brewer
from coffeemaker.src.model.Grinder import Grinder
from coffeemaker.src.model.MilkTank import MilkTank
from coffeemaker.src.model.WaterTank import WaterTank
from coffeemaker.src.model.CoffeeBeansTank import CoffeeBeansTank
from coffeemaker.src.model.WeightSensor import WeightSensor
from coffeemaker.src.controller.CoffeeMainViewController import CoffeeMainViewController
from coffeemaker.src.resources.CoffeeTypesDictionary import CoffeeTypesDictionary

# create the weight sensors for each tank
water_sensor, coffee_beans_sensor, milk_sensor = WeightSensor(), WeightSensor(), WeightSensor()

# create the tanks and assign their respective sensors
water_tank = WaterTank(water_sensor)
coffee_beans_tank = CoffeeBeansTank(coffee_beans_sensor)
milk_tank = MilkTank(milk_sensor)

# set the sensor reading to the max possible tank capacity
water_sensor.current_reading = water_tank.max_content_weight
milk_sensor.current_reading = milk_tank.max_content_weight
coffee_beans_sensor.current_reading = coffee_beans_tank.max_content_weight

# create the grinder
grinder = Grinder(coffee_beans_tank)

# create the brewer
brewer = Brewer(water_tank, milk_tank, grinder)

# initialize the coffee dict resource
coffee_types_dictionary = CoffeeTypesDictionary()

# create the main controller and display the associated view
coffee_main_view_controller = CoffeeMainViewController(brewer, coffee_types_dictionary)
coffee_main_view_controller.show_main_view()
