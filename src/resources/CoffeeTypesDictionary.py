from coffeemaker.src.resources.ICoffeeTypesDictionary import ICoffeeTypesDictionary
import json


class CoffeeTypesDictionary(ICoffeeTypesDictionary):
    """
    Coffee type dictionary. Contains different coffee types and their respective ingredient lists.
    """

    def __init__(self):
        self.coffee_types_dictionary = {}
        self.read_from_file()

    def get_all(self):
        coffee_types = []
        for key in self.coffee_types_dictionary:
            coffee_types.append(key)
        return coffee_types

    def get_ingredients(self, coffee_type):
        return self.coffee_types_dictionary[coffee_type]

    def read_from_file(self, filename="coffeemaker/src/resources/coffee_types.json"):
        with open(filename, "r") as in_file:
            self.coffee_types_dictionary = json.load(in_file)

    def save_to_file(self, filename="coffeemaker/src/resources/coffee_types.json"):
        with open(filename, "w") as out_file:
            self.coffee_types_dictionary = json.dump(self.coffee_types_dictionary, out_file)
