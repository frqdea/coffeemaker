from abc import ABC, abstractmethod


class ICoffeeTypesDictionary(ABC):

    @abstractmethod
    def get_all(self):
        pass

    @abstractmethod
    def get_ingredients(self, coffee_type):
        pass
