from coffeemaker.src.model.ISensor import ISensor


class WeightSensor(ISensor):
    """
    Weight sensor class. Runs as a daemon and updates the reading periodically.
    When polled, the reading is returned to the connected component (i.e. water tank).
    """

    def __init__(self, current_reading=0):
        self.current_reading = current_reading

    def get_reading(self):
        """
        Get the weight reading from the sensor.
        :return:
        """
        if self.current_reading < 0:
            raise ValueError("Weight reading below 0.")
        return self.current_reading
