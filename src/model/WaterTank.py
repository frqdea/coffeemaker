from coffeemaker.src.model.InsufficientResourceError import InsufficientResourceError
from coffeemaker.src.model.ITank import ITank
import time


class WaterTank(ITank):
    """
    Water Tank class. Represents the behaviour of a tank containing water in a coffee maker.
    Connected to a weight sensor that updates itself periodically.
    When the tank receives a request to dump water, the sensor is polled and the ingredients are dumped
    if available.
    """

    def __init__(self, water_sensor, max_content_weight=1000):
        self.water_sensor = water_sensor
        self.max_content_weight = max_content_weight

    def get_contents(self, weight):
        """
        Dump the requested amount of water.
        If the sensor reading is less than the requested amount, raise an exception informing of the issue.
        :param weight:
        :return:
        """
        if weight < 0:
            raise ValueError("Requested negative water weight.")
        elif self.water_sensor.get_reading() >= weight:
            print("Dumping " + str(weight) + " grams of water into the brewer.")
            time.sleep(1)
        else:
            raise InsufficientResourceError("Not enough water in the tank!")
