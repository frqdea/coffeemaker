from abc import ABC, abstractmethod


class ITank(ABC):

    @abstractmethod
    def get_contents(self, weight):
        pass
