from coffeemaker.src.model.InsufficientResourceError import InsufficientResourceError
from coffeemaker.src.model.ITank import ITank
import time


class CoffeeBeansTank(ITank):
    """
    Coffee Beans Tank class. Represents the behaviour of a tank containing coffee beans in a coffee maker.
    Connected to a weight sensor that updates itself periodically.
    When the tank receives a request to dump beans, the sensor is polled and the ingredients are dumped
    if available.
    """

    def __init__(self, coffee_beans_sensor, max_content_weight=250):
        self.coffee_beans_sensor = coffee_beans_sensor
        self.max_content_weight = max_content_weight

    def get_contents(self, weight):
        """
        Dump the requested amount of coffee beans.
        If the sensor reading is less than the requested amount,
        raise and exception informing of the issue.
        :param weight:
        :return:
        """
        if weight < 0:
            raise ValueError("Requested negative coffee beans weight.")
        elif self.coffee_beans_sensor.get_reading() >= weight:
            print("Dumping " + str(weight) + " grams of beans into the grinder.")
            time.sleep(1)
        else:
            raise InsufficientResourceError("Not enough beans in the tank!")
