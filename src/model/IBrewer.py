from abc import ABC, abstractmethod


class IBrewer(ABC):

    @abstractmethod
    def brew(self, coffee_ingredients):
        pass
