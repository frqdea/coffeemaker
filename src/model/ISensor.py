from abc import ABC, abstractmethod


class ISensor(ABC):

    @abstractmethod
    def get_reading(self):
        pass
