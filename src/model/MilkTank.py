from coffeemaker.src.model.InsufficientResourceError import InsufficientResourceError
from coffeemaker.src.model.ITank import ITank
import time


class MilkTank(ITank):
    """
    Milk Tank class. Represents the behaviour of a tank containing milk in a coffee maker.
    Connected to a weight sensor that updates itself periodically.
    When the tank receives a request to dump milk, the sensor is polled and the ingredients are dumped
    if available.
    """

    def __init__(self, milk_sensor, max_content_weight=500):
        self.milk_sensor = milk_sensor
        self.max_content_weight = max_content_weight

    def get_contents(self, weight):
        """
        Dump the requested amount of milk.
        If the sensor reading is less than the requested amount,
        raise and exception informing of the issue.
        :param weight:
        :return:
        """
        if weight < 0:
            raise ValueError("Requested negative milk weight.")
        elif self.milk_sensor.get_reading() >= weight:
            print("Dumping " + str(weight) + " grams of milk into the brewer.")
            time.sleep(1)
        else:
            raise InsufficientResourceError("Not enough milk in the tank!")
