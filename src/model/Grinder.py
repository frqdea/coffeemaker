from coffeemaker.src.model.IGrinder import IGrinder
import time


class Grinder(IGrinder):
    """
    Grinder class. Grinds coffee beans received from the coffee bean tank.
    After grinding immediately dumps grounds into brewer.
    """

    def __init__(self, coffee_beans_tank):
        self.coffee_beans_tank = coffee_beans_tank

    def get_coffee_grounds(self, weight):
        """
        Request the appropriate amount of coffee beans from the coffee bean tank, then grind it and dump it.
        :param weight:
        :return:
        """
        self.coffee_beans_tank.get_contents(weight)
        print("Grinding " + str(weight) + " grams of coffee beans.")
        time.sleep(1)
