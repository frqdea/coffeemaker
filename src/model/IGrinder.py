from abc import ABC, abstractmethod


class IGrinder(ABC):

    @abstractmethod
    def get_coffee_grounds(self, weight):
        pass
