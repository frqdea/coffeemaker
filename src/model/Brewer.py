from coffeemaker.src.model.IBrewer import IBrewer
import time


class Brewer(IBrewer):
    """
    Brewer class. Connected to the grinder and 1 tank of milk and water.
    Receives an ingredient list, then requests the appropriate amounts from each connected container.
    """

    def __init__(self, water_tank, milk_tank, grinder):
        self.water_tank = water_tank
        self.milk_tank = milk_tank
        self.grinder = grinder

    def brew(self, coffee_ingredients):
        """
        Signal all the connected resource tanks to dump
        the appropriate amounts of water, milk and coffee grounds,
        then brew the coffee
        :param coffee_ingredients:
        :return:
        """
        water_weight = coffee_ingredients["water"]
        grounds_weight = coffee_ingredients["grounds"]
        milk_weight = coffee_ingredients["milk"]

        if not isinstance(milk_weight, int) and isinstance(water_weight, int) and isinstance(grounds_weight, int):
            raise ValueError("Non numeric weight value.")

        self.water_tank.get_contents(water_weight)
        self.grinder.get_coffee_grounds(grounds_weight)
        if milk_weight > 0:
            self.milk_tank.get_contents(milk_weight)

        print("Brewing your coffee.")
        time.sleep(1)
        print("Done!")
        return True
