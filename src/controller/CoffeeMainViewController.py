from coffeemaker.src.view.CoffeeMainView import CoffeeMainView


class CoffeeMainViewController:
    """
    Main View controller. Relays info between the view and the app logic.
    """

    def __init__(self, brewer, coffee_types_dictionary):
        self.brewer = brewer
        self.coffee_types_dictionary = coffee_types_dictionary
        self.coffee_main_view = CoffeeMainView(self)

    def send_coffee_type(self, coffee_type):
        """
        Get the ingredient list for the provided coffee type. Signal the brewer to brew the coffee.
        :param coffee_type:
        :return:
        """
        ingredients = self.coffee_types_dictionary.get_ingredients(coffee_type)
        return self.brewer.brew(ingredients)

    def show_main_view(self):
        """
        Display the main view.
        :return:
        """
        coffee_types = self.coffee_types_dictionary.get_all()
        self.coffee_main_view.main_menu(coffee_types)
